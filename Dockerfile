#########################
# Build React app       #
#########################
FROM node:16.8-alpine as builder

WORKDIR /app
COPY . .

RUN yarn
RUN yarn build

###########################
# Build deployable image  #
###########################
FROM nginx:1.21.4-alpine

COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
COPY ./nginx.config /etc/nginx/conf.d/default.conf
CMD [ "nginx", "-g", "daemon off;" ]